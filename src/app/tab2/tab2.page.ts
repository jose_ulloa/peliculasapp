import { Component } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { Movie } from '../interfaces/interface';
import { ModalController } from '@ionic/angular';
import { DetailComponent } from '../components/detail/detail.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  searching: boolean = false;
  txtSearch: string = "";
  movies: Movie[] = [];
  ideas: string[] = ['Spiderman', 'Superman', 'Avenger', 'Capitán América'];

  constructor(private movieService: MovieService, public modalController: ModalController) {}

  search(event){
    const value = event.detail.value;

    if (value !== ''){
      this.searching = true;
      this.movieService.searchMovie(value)
      .subscribe( res => {
       this.movies = res['results'];
       this.searching = false;
      });
    }  else {
      this.movies = [];
      this.txtSearch = '';
    }
  }

  async showDetail(id: string){
    const modal = await this.modalController.create({
      component: DetailComponent,
      componentProps: {
        id
      }
    });
    return await modal.present();
  }

}
