import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterImage'
})
export class FilterImagePipe implements PipeTransform {

  transform(movie: any[]): any {
    return movie.filter( movie => {
      return movie.backdrop_path;
    });
  }

}
