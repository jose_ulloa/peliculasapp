import { Component } from '@angular/core';
import { MovieDetail, Genre } from '../interfaces/interface';
import { DataLocalService } from '../services/data-local.service';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page{

  movies: MovieDetail[] = [];
  genres: Genre[] = [];

  genreFavorite: any[] = [];

  constructor(
    private dataLocal: DataLocalService, 
    private movieService: MovieService) {}
  

  async ionViewWillEnter(){
    this.movies =  await this.dataLocal.loadFavorites();
    this.genres = await this.movieService.getGenres();

    this.moviePerGenre();
  }

  moviePerGenre(){
    this.genreFavorite = [];

    this.genres.forEach( genreArray => {
        this.genreFavorite.push({
          genreName: genreArray.name,
          movies: this.movies.filter (movie => {
            return movie.genres.find( genre => genre.id === genreArray.id);
          })
      });     
    });
  }
}