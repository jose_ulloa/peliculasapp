import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { MovieService } from '../../services/movie.service';
import { Identifiers } from '@angular/compiler';
import { MovieDetail, Actors, Cast } from '../../interfaces/interface';
import { DataLocalService } from '../../services/data-local.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  @Input() id: string;
  movieDetail: MovieDetail = {};
  actors: Cast[] = [];
  numberCharacters: number = 150;
  isFavorite: boolean;
  iconFavorite: string;

  slideOptActors = {
    slidesPerView: 3.3,
    freeMode: true,
    spaceBetwwen: -5
  };

  constructor(
    private movieService: MovieService, 
    private modalController: ModalController,
    private dataLocalService: DataLocalService,
    private toastController: ToastController){}

  async ngOnInit() {
    this.isFavorite = await this.dataLocalService.isExit(this.id);
  
    this.movieService.getMovieDetail(this.id)
    .subscribe(
      res => {
        this.movieDetail = res;
      }
    );

    this.movieService.getMovieActors(this.id)
    .subscribe(
      res => {
        this.actors = res.cast;
      }
    );

    if (this.isFavorite){
      this.iconFavorite = "star";
    } else {
      this.iconFavorite = "star-outline";
    }
  }

  back(){
    this.modalController.dismiss();
  }

  async favorite(){
    this.isFavorite = await this.dataLocalService.isExit(this.id);
    let sms: string = '';

    if(this.isFavorite){
      this.dataLocalService.deleteMovie(this.movieDetail);
      this.iconFavorite = "star-outline";
      sms = 'Eliminado de favorito';

    } else {
      this.dataLocalService.saveMovie(this.movieDetail);
      this.iconFavorite = "star";
      sms = "Agregado a favorito" ;
    }

    const toast = await this.toastController.create({
      message: sms,
      duration: 2000
    });
    toast.present();
  }

}