import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../../interfaces/interface';
import { ModalController } from '@ionic/angular';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-slideshow-backdrop',
  templateUrl: './slideshow-backdrop.component.html',
  styleUrls: ['./slideshow-backdrop.component.scss'],
})
export class SlideshowBackdropComponent implements OnInit {

  @Input() movies: Movie[] = [];

  slidesOptions = {
    slidesPerView: 1.1,
    freeMode: true
  };

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  async showDetail(id: string){
    const modal = await this.modalController.create({
      component: DetailComponent,
      componentProps: {
        id
      }
    });
    return await modal.present();
  }

}
