import { Component, OnInit, Input } from '@angular/core';
import { Movie } from '../../interfaces/interface';
import { ModalController } from '@ionic/angular';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-slideshow-poster',
  templateUrl: './slideshow-poster.component.html',
  styleUrls: ['./slideshow-poster.component.scss'],
})
export class SlideshowPosterComponent implements OnInit {

  @Input() movies: Movie[] = [];

  slidesOptions = {
    slidesPerView: 3.3,
    freeMode: true
  };

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  async showDetail(id: string){
    const modal = await this.modalController.create({
      component: DetailComponent,
      componentProps: {
        id
      }
    });
    return await modal.present();
  }

}
