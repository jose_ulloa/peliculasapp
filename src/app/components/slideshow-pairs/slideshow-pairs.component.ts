import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../interfaces/interface';
import { ModalController } from '@ionic/angular';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-slideshow-pairs',
  templateUrl: './slideshow-pairs.component.html',
  styleUrls: ['./slideshow-pairs.component.scss'],
})
export class SlideshowPairsComponent implements OnInit {

  @Input() movies: Movie[] = [];
  @Output() loadMore = new EventEmitter();

  slidesOptions = {
    slidesPerView: 3.3,
    freeMode: true,
    spaceBetween: -10
  };

  constructor(public modalController: ModalController) { }

  ngOnInit() {}

  onClick(){
    this.loadMore.emit();
  }

  async showDetail(id: string){
    const modal = await this.modalController.create({
      component: DetailComponent,
      componentProps: {
        id
      }
    });
    return await modal.present();
  }
}
