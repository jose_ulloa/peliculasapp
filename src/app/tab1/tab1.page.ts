import { Component, OnInit } from '@angular/core';
import { MovieService } from '../services/movie.service';
import { Movie } from '../interfaces/interface';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  residentMovies: Movie[] = [];
  popular: Movie[] = [];
  
  constructor(private moviesService: MovieService) {}

  ngOnInit(){
    this.moviesService.getFeature()
    .subscribe( res => {
      this.residentMovies = res.results;
    });

    this.getPopular();
  }

  loadMore(){
    this.getPopular();
  }

  getPopular(){
    this.moviesService.getPopular()
    .subscribe( res => {
      const arrTemp = [... this.popular, ...res.results];
      this.popular = arrTemp;
    });
  }
}
