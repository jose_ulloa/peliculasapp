import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MovieDetail } from '../interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  movieDetails: MovieDetail[] = [];

  constructor(private storage: Storage) { 
  }

  async isExit(movieDetailId){
    await this.loadFavorites();
    const exit = this.movieDetails.find(data => data.id === movieDetailId);
    return (exit)? true: false;
  }
  
  saveMovie(movieDetail: MovieDetail){
    this.movieDetails.unshift(movieDetail);
    this.storage.set('movies', this.movieDetails);
  }

  async loadFavorites(){
    const favorites = await this.storage.get('movies');
    this.movieDetails = favorites || [];
    return this.movieDetails;
  }
 
  deleteMovie(movieDetail: MovieDetail){
    this.movieDetails = this.movieDetails.filter( data => data.id !== movieDetail.id);
    this.storage.set('movies', this.movieDetails);
  }
}
