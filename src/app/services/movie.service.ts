import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResultMDB, MovieDetail, Actors, Genre } from '../interfaces/interface';
import { environment } from '../../environments/environment';

const URL    = environment.url;
const apiKey = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private httpClient: HttpClient) { }
  private popularPage = 0;

  private runQuery<T>(query: string){
    query = URL + query;
    query += `&api_key=${apiKey}&language=es&include_image_language=es`;
    return this.httpClient.get<T>(query);
  } 

  getPopular(){
    this.popularPage++;

    const query = `discover/movie?sort_by=popularity.desc&page=${this.popularPage}`;
    return this.runQuery<ResultMDB>(query);
  }

  getFeature(){

    const today = new Date();
    const lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
    const month = today.getMonth();

    let monthString: any;

    if(month < 10){
      monthString = '0' + month;
    } else {
      monthString = month;
    }

    const startDate = `${today.getFullYear()}-${monthString}-01`;
    const endDate = `${today.getFullYear()}-${monthString}-${lastDay}`;

    return this.runQuery<ResultMDB>(`discover/movie?primary_release_date.gte=${ startDate }&primary_release_date.lte=${ endDate }`);
  }

  getMovieDetail(id: string){
    return this.runQuery<MovieDetail>(`movie/${ id }?a=a`);
  }

  getMovieActors(id: string){
    return this.runQuery<Actors>(`movie/${ id }/credits?a=a`);
  }

  searchMovie(nameMovie: string){
    return this.runQuery(`search/movie?query=${ nameMovie }`);
  }

  getGenres(): Promise<Genre[]> {
    return new Promise( resolve => {
      this.runQuery("genre/movie/list?a=1").subscribe(
        res => {
          resolve(res['genres']);
        }
      );
    });    
  }
}